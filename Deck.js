var Card = require('./Card.js' );
var Constants = require('./Constants.js' );


function Deck() {
  this.cards = [];
  this.card = -1;
  count = 0;
  for (c=0; c < Constants.CARDS.length; c++) {
    for (s=0; s < Constants.SUITS.length; s++) {
      card = new Card(c, s);
      this.cards.push(card);
      count++;
    }
  }
}

Deck.prototype.shuffle = function shuffle() {
  this.card = 0;
  array = this.cards;

  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
}

Deck.prototype.print = function print() {
  console.log("Length: " + this.cards.length);

  for (i; i < this.cards.length; i++) {
    this.cards[i].print();
  }
}

Deck.prototype.dealCard = function () {
  if (this.card == -1) {
    this.shuffle();
    this.card = 0;
  }

  c = this.card;
  this.card++;

  console.log("dealing " + this.cards[c].name());

  return this.cards[c];
}

module.exports = Deck;
