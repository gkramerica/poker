var Card = require('./Card.js' );

function Hand(c1, c2) {
  if (c1.compare(c2) == 1) {
    this.card1 = c1;
    this.card2 = c2;
  } else {
    this.card1 = c2;
    this.card2 = c1;
  }
}

Hand.prototype.contains = function contains(card) {
  return this.card1.equal(card) || this.card2.equal(card);
}

Hand.prototype.print = function print() {
  console.log(this.card1.name() + this.card2.name());
}

module.exports = Hand;
