var Card = require('./Card.js' );
var Deck = require('./Deck.js' );

function Game(deck, hand1, hand2) {
  this.board = [];

  this.board.push(deck.dealCard());
  this.board.push(deck.dealCard());
  this.board.push(deck.dealCard());
  deck.dealCard();
  this.board.push(deck.dealCard());
  deck.dealCard();
  this.board.push(deck.dealCard());
}


Game.prototype.print = function print() {
  console.log(this.board.length);

  s = "";
  for (i=0; i < this.board.length; i++) {
    s += this.board[i].name() + " ";
  }

  console.log(s);
};

module.exports = Game;
