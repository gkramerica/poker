
var Constants = require('./Constants.js' );
var Card = require('./Card.js' );
var Hand = require('./Hand.js' );
var PokerHand = require('./PokerHand.js' );
var Deck = require('./Deck.js' );
var Game = require('./Game.js' );

d = new Deck();

h1 = new Hand(d.dealCard(), d.dealCard());
h2 = new Hand(d.dealCard(), d.dealCard());

h1.print();
h2.print();

g = new Game(d, h1, h2);

g.print();

cards1 = g.board.slice();
cards1.push(h1.card1);
cards1.push(h1.card2);

ph1 = new PokerHand(cards1);

ph1.print();
