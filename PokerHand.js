var Card = require('./Card.js' );
var Constants = require('./Constants.js' );

function PokerHand(c) {
  c.sort(function(a,b) {
    return -a.compare(b);
  });


  this.cards = c;
  this.bestHand = [];
  this.rank = 0;

  findBestHand(this);
}


PokerHand.prototype.print = function print() {
  for (i=0; i < this.bestHand.length; i++) {
    this.bestHand[i].print();
  }

  console.log(Constants.RANKS[this.rank]);
}


PokerHand.prototype.rankName = function rankName() {
  return Constants.RANKS[this.rank];
}


module.exports = PokerHand;



function findBestHand(hand) {
  findDuplicates(hand);

  if (hand.rank < 5) {
    flush = findBestFlush(hand.cards);
    straight = findBestStraight(hand.cards);

    if (flush != null) {
      hand.rank = 5;
      hand.bestHand = flush;
      return;
    }
    if (straight != null) {
        hand.rank = 4;
        hand.bestHand = straight;
        return;
    }
  }

  if (hand.rank == 0) {
    hand.bestHand = hand.cards.splice(0,5);
  }
}

function findDuplicates(hand) {
  pair1 = -1;
  pair2 = -1;
  trips = -1;

  len = hand.cards.length;

  for (i=0; i<len-1; i++) {
    if (hand.cards[i].card == hand.cards[i+1].card) {
      if (len > i + 2 && hand.cards[i].card == hand.cards[i+2].card) {
        if (len > i + 3 && hand.cards[i].card == hand.cards[i+3].card) {
          hand.rank = 7;
          for (z=0; z < 4; z++) {
            hand.bestHand.push(hand.cards[z + i]);
          }
          if (i == 0) {
            hand.bestHand.push(hand.cards[4]);
          } else {
            hand.bestHand.push(hand.cards[0]);
          }
          return;
        } else if (trips == -1) {
          trips = i;
          i += 1;
        } else if (pair1 == -1) {
          pair1 = i;
        }
      } else if (pair1 == -1) {
        pair1 = i;
      } else if (pair2 == -1) {
        pair2 = i;
      }
    }
  }

  if (trips == -1) {
    if (pair1 > -1) {
      for (z=0; z < 2; z++) {
        hand.bestHand.push(hand.cards[z + pair1]);
      }
      if (pair2 > -1) {
        for (z=0; z < 2; z++) {
          hand.bestHand.push(hand.cards[z + pair2]);
        }
        hand.rank = 2;
      } else {
        hand.rank = 1;
      }
    }
  } else {
    for (z=0; z < 3; z++) {
      hand.bestHand.push(hand.cards[z + trips]);
    }
    if (pair1 > -1) {
      for (z=0; z < 2; z++) {
        hand.bestHand.push(hand.cards[z + pair1]);
      }
      hand.rank = 6;
      return;
    } else {
      hand.rank = 3;
    }
  }

  if (hand.rank > 0) {
    for (i=0; i < len; i++) {
      if (i == pair1 || i == pair2) {
        i += 1;
      } else if (i == trips) {
        i += 2;
      } else {
        hand.bestHand.push(hand.cards[i]);
        if (hand.bestHand.length == 5) {
          return;
        }
      }
    }
  }


}

function findBestFlush(cards) {
  for (s=0; s < Constants.SUITS.length; s++) {
    hand = [];

    bad = 0;

    for (c=0; c < cards.length; c++) {
      if (cards[c].suit == s) {
        hand.push(cards[c]);
        if (hand.length == 5) {
          return hand;
        }
      } else {
        bad++;
        if (bad > 2) {
          break;
        }
      }
    }
    if (hand.length >= 3) {
      // 3 or 4 of this suit, so no other flush possible
      return null;
    }

  }
  return null;
}

function findBestStraight(cards) {
  h = [];
  h.push(cards[0]);

  for (s=0; s < cards.length - 1; s++) {
    if (cards[s].card == cards[s + 1].card+1) {
      h.push(cards[s + 1]);

      if (h.length == 5) {
        return h;
      }
    } else if (cards[s].card == cards[s + 1].card) {
      // Pair, skip it
    } else {
      h = [];
      h.push(cards[s + 1]);
    }
  }

  if (h.length == 4) {
    if (h[h.length - 1].card == 0) {
      if (cards[0].card == 12) {
        // Wheel!!!
        h.push(cards[0]);
        return h;
      }
    }
  }
  return null;
}
