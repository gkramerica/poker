var assert = require('assert');
var Card = require('../Card');
var PokerHand = require('../PokerHand');

describe('Card', function() {
  describe('name()', function() {
    it('should return 2c', function() {
      c = new Card(0,0);
      assert.equal("2c", c.name());
    });

    it('should return Jh', function() {
      c = new Card(9,2);
      assert.equal("Jh", c.name());
    });
  });

  describe('Create From Name', function() {
    it('should return Ah', function() {
      c = new Card("Ah");
      assert.equal("Ah", c.name());
      assert.equal(12, c.card);
      assert.equal(2, c.suit);
    });
    it('should return 3s', function() {
      c = new Card("3s");
      assert.equal("3s", c.name());
      assert.equal(1, c.card);
      assert.equal(3, c.suit);
    });

  });

});

describe('PokerHand', function() {
  describe('Straight', function() {
    it('hand rank', function() {
      cards = [];
      cards.push(new Card(0,1));
      cards.push(new Card(1,0));
      cards.push(new Card(2,1));
      cards.push(new Card(8,1));
      cards.push(new Card(3,3));
      cards.push(new Card(4,1));
      cards.push(new Card(8,3));

      h = new PokerHand(cards);

      assert.equal("Straight", h.rankName());
    });

  });
  describe('Wheel', function() {
    it('hand rank', function() {
      cards = [];
      cards.push(new Card(0,1));
      cards.push(new Card(1,0));
      cards.push(new Card(2,1));
      cards.push(new Card(8,1));
      cards.push(new Card(3,3));
      cards.push(new Card(12,1));
      cards.push(new Card(8,3));

      h = new PokerHand(cards);

      assert.equal("Straight", h.rankName());
    });
  });

  describe('Boat', function() {
    it('hand rank', function() {
      cards = [];
      cards.push(new Card(12,0));
      cards.push(new Card(7,1));
      cards.push(new Card(2,1));
      cards.push(new Card(8,1));
      cards.push(new Card(12,2));
      cards.push(new Card(12,1));
      cards.push(new Card(8,3));

      h = new PokerHand(cards);

      assert.equal("Full House", h.rankName());
    });
  });

  describe('Flush', function() {
      it('hand rank', function() {
        cards = [];
        cards.push(new Card(12,0));
        cards.push(new Card(7,1));
        cards.push(new Card(2,1));
        cards.push(new Card(8,1));
        cards.push(new Card(11,1));
        cards.push(new Card(12,1));
        cards.push(new Card(8,3));

        h = new PokerHand(cards);

        assert.equal("Flush", h.rankName());
      });
    });

 describe('Quads', function() {
    it('hand rank', function() {
      cards = [];
      cards.push(new Card(12,0));
      cards.push(new Card(3,2));
      cards.push(new Card(3,1));
      cards.push(new Card(3,0));
      cards.push(new Card(11,2));
      cards.push(new Card(7,1));
      cards.push(new Card(3,3));

      h = new PokerHand(cards);

      assert.equal("4 of a Kind", h.rankName());
    });
  });

   describe('Trips', function() {
      it('hand rank', function() {
        cards = [];
        cards.push(new Card(12,0));
        cards.push(new Card(7,2));
        cards.push(new Card(3,1));
        cards.push(new Card(3,0));
        cards.push(new Card(11,2));
        cards.push(new Card(6,1));
        cards.push(new Card(3,3));

        h = new PokerHand(cards);
        assert.equal("3 of a Kind", h.rankName());
      });
    });

   describe('Two Pair', function() {
      it('hand rank', function() {
        cards = [];
        cards.push(new Card(12,0));
        cards.push(new Card(7,2));
        cards.push(new Card(3,1));
        cards.push(new Card(3,0));
        cards.push(new Card(11,2));
        cards.push(new Card(7,1));
        cards.push(new Card(8,3));

        h = new PokerHand(cards);

        assert.equal("2 Pair", h.rankName());
      });
    });
   describe('Pair', function() {
      it('hand rank', function() {
        cards = [];
        cards.push(new Card(12,0));
        cards.push(new Card(7,2));
        cards.push(new Card(2,1));
        cards.push(new Card(3,1));
        cards.push(new Card(11,2));
        cards.push(new Card(9,1));
        cards.push(new Card(9,3));

        h = new PokerHand(cards);

        assert.equal("Pair", h.rankName());
      });
    });
     describe('High Card', function() {
        it('hand rank', function() {
          cards = [];
          cards.push(new Card(12,0));
          cards.push(new Card(7,2));
          cards.push(new Card(2,1));
          cards.push(new Card(3,1));
          cards.push(new Card(11,2));
          cards.push(new Card(9,1));
          cards.push(new Card(8,3));

          h = new PokerHand(cards);

          assert.equal("High Card", h.rankName());
        });
      });

      describe('StraightFlush', function() {
        it('hand rank', function() {
          cards = [];
          cards.push(new Card(0,1));
          cards.push(new Card(1,1));
          cards.push(new Card(2,1));
          cards.push(new Card(8,1));
          cards.push(new Card(3,1));
          cards.push(new Card(4,1));
          cards.push(new Card(8,3));

          h = new PokerHand(cards);

          assert.equal("Straight Flush", h.rankName());
        });

      });
      describe('WheelStraightFlush', function() {
        it('hand rank', function() {
          cards = [];
          cards.push(new Card(1,1));
          cards.push(new Card(2,1));
          cards.push(new Card(0,1));
          cards.push(new Card(8,1));
          cards.push(new Card(3,1));
          cards.push(new Card(12,1));
          cards.push(new Card(8,3));
    
          h = new PokerHand(cards);

          assert.equal("Straight Flush", h.rankName());
        });
      });
});
