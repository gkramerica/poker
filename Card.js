
var Constants = require('./Constants.js' );


function Card(card, suit) {
  if (arguments.length == 1) {
    createFromName(this, card);
  } else {
    this.card=card;
    this.suit=suit;
  }
}

function createFromName(card, name) {
  c = name.substring(0,1);
  s = name.substring(1,2);
  for (i=0; i < Constants.CARDS.length; i++) {
     if (Constants.CARDS[i] == c) {
       card.card = i;
       break;
     }
  }

  for (i=0; i < Constants.SUITS.length; i++) {
     if (Constants.SUITS[i] == s) {
       card.suit = i;
       break;
     }
  }
}

Card.prototype.equal = function equal(c) {
  return this.card == c.card && this.suit == c.suit;
}

Card.prototype.compare = function compare(c) {
  if (this.card > c.card) {
    return 1;
  }
  if (this.card < c.card) {
    return -1;
  }
  if (this.suit > c.suit) {
    return 1;
  }
  if (this.suit < c.suit) {
    return 1;
  }
  return 0;
}

Card.prototype.name = function name() {
  return Constants.CARDS[this.card] + Constants.SUITS[this.suit];
}

Card.prototype.print = function print() {
  console.log(this.name());
}

module.exports = Card;
